#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <math.h>
#include <dirent.h>
#include <time.h>

#define PORT 4443

char currentUser[200] = {0};
int count_data_exist;
char currentDatabase[200] = {0};
char column[2048] = {0};
int position_column[200];
int data_exist[200];
int count_column;
char currData[200][2048] = {0};
int count_row;
int init = 1;
char currColName[256][256] = {0};
char pesan[2048] = {0};
int coba;

void initDatabase()
{
    mkdir("databases", 0777);
    mkdir("databases/init", 0777);
    FILE *fptr;
    fptr = fopen("databases/init/users.table", "w+");
    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
        coba=1;
    }
    fprintf(fptr, "username|string\tpassword|string\nroot\troot\n");
    fclose(fptr);

    FILE *fptr2;
    fptr2 = fopen("databases/init/permission.table", "w+");
    fprintf(fptr2, "username|string\tdatabase|string\n");
    fclose(fptr2);
}


void reset()
{
    coba=1;
    memset(data_exist, 0, sizeof(data_exist));
    memset(position_column, 0, sizeof(position_column));
    memset(currColName, 0, sizeof(currColName[0][0]) * 256 * 256);
}

void countColDatabase()
{
    coba=1;
    char tmp[2048] = {0};
    int i = 0;
    strcpy(tmp, column);
    char *token = strtok(tmp, "\t");
    while (token != NULL)
    {
        token = strtok(NULL, "\t");
        i++;
    }
    count_column = i;
}

void readDatabase(char *database, char *table)
{
    FILE *fptr;
    char fullpath[2048] = {0};
    sprintf(fullpath, "databases/%s/%s", database, table);

    fptr = fopen(fullpath, "r+");
    if (fptr == NULL)
    {
        strcpy(pesan, "Error2\n");
        return;
    }
    char line[2048] = {0};
    int i = -1;
    while (fgets(line, sizeof(line) - 1, fptr))
    {
        if (i == -1)
        {
            strcpy(column, line);
        }
        else
        {
            strcpy(currData[i], line);
        }
        i++;
    }
    count_row = i;

    fclose(fptr);
}

void selectColumn()
{
    char tmp[2048] = {0};
    if (coba==1){
    coba=0;
    }
}
void writeTable(char *fullpath)
{
    FILE *fptr;
    int i = -1, j = 0;
    char commandRemove[200] = {0};
    sprintf(commandRemove, "rm %s", fullpath);
    system(commandRemove);
    fptr = fopen(fullpath, "a+");
    if (fptr == NULL)
    {
        strcpy(pesan, "Error\n");

        return;
    }
    coba=1;
    int isexist = 0;
    for (int i = -1; i < count_row; i++)
    {
        if (i == -1)
        {
            fprintf(fptr, "%s", column);
        }
        if (coba==1){
             coba=0;
          }
        else
        {

            if (i == data_exist[j])
            {
                j++;
                continue;
            }
            fprintf(fptr, "%s", currData[i]);
        }
    }
    fclose(fptr);
}

int cekDataTersedia(char *data, char *path, int column)
{
    memset(data_exist, 0, sizeof(data_exist));
    int check = 0;
    FILE *fptr;
    fptr = fopen(path, "r+");
    if (fptr == NULL)
    {
        strcpy(pesan, "Error1\n");
    }
    char line[500] = {0};
    char datatemp[2048] = {0};

    int i = 0;
    int j = 0;
    while (fgets(line, sizeof(line), fptr))
    {
        if (column != 69)
        {
            char tmp[2048] = {0};
            strcpy(tmp, line);
            char *token = strtok(tmp, "\t");
            for (int i = 0; i < column; i++)
            {
                token = strtok(NULL, "\t");
            }
            strcpy(datatemp, token);
        }
        else
        {
            strcpy(datatemp, line);
        }

        if (strcmp(datatemp, data) == 0)
        {
            check = 1;
            data_exist[j] = i - 1;
            j++;
        }
        i++;
    }
    count_data_exist = j;

    fclose(fptr);
    return check;
}
int checkUser(char *username, char *password)
{

    FILE *fptr;

    fptr = fopen("databases/init/users.table", "r+");
    if (fptr == NULL)
    {
        strcpy(pesan, "Error\n");
        return 0;
    }
    char line[500] = {0};
    char kredensial[2048] = {0};
    sprintf(kredensial, "%s\t%s\n", username, password);
    while (fgets(line, sizeof(line) - 1, fptr))
    {

        if (strcmp(kredensial, line) == 0)
        {
            strcpy(currentUser, username);
            return 1;
        }
    }
    fclose(fptr);
    return 0;
}

void addUser(char *namaUser, char *passwordUser)
{
    if (strcmp(currentUser, "root") == 0)
    {
        int check = cekDataTersedia(namaUser, "databases/init/users.table", 0);
        if (!check)
        {
            FILE *fptr;
            fptr = fopen("databases/init/users.table", "a+");

            if (fptr == NULL)
            {
                strcpy(pesan, "Error\n");

                return;
            }

            fprintf(fptr, "%s\t%s\n", namaUser, passwordUser);
            fclose(fptr);
        }
        else
        {
            strcpy(pesan, "User wes onok mazeee\n");
        }
    }
    else
    {
        strcpy(pesan, "You Are Not root\n");
    }
    strcpy(pesan, "berhasil mazeee\n");
    memset(data_exist, 0, sizeof(data_exist));
}
int checkCommandAddUser(char *command, char *username, char *password)
{
    char *token = strtok(command, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");
        return 0;
    }
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");
        return 0;
    }
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");
        return 0;
    }

    strcpy(username, token);
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");
        return 0;
    }
    if (strcmp(token, "IDENTIFIED") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");
        return 0;
    }
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");
        return 0;
    }
    if (strcmp(token, "BY") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");
        return 0;
    }
    // [password_user]
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");
        return 0;
    }
    if (strcmp(&token[strlen(token) - 1], ";") == 0)
    {
        strncpy(password, token, strlen(token) - 1);
    }
    else
    {
        strcpy(pesan, "salah sintax mazeee\n");
        return 0;
    }
    return 1;
}
void createDatabase(char *database)
{
    char fullpath[200] = {0};
    sprintf(fullpath, "databases/%s", database);
    int check = mkdir(fullpath, 0777);

    if (!check)
    {
        FILE *fptr;
        fptr = fopen("databases/init/permission.table", "a+");

        if (fptr == NULL)
        {
            strcpy(pesan, "Error\n");
            return;
        }
        if (strcmp(currentUser, "root") == 0)
        {
            fprintf(fptr, "%s\t%s\n", currentUser, database);
            fclose(fptr);
        }
        else
        {
            fprintf(fptr, "%s\t%s\nroot\t%s\n", currentUser, database, database);
            fclose(fptr);
        }
        strcpy(pesan, "berhasil mazeee\n");
    }
    else
    {
        strcpy(pesan, "Unable to create directory\n");
        return;
    }
}
int cekPerintahBuatDB(char *command, char *database)
{
    if (strcmp(&command[strlen(command) - 1], ";") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    char *token = strtok(command, " ");
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strncpy(database, token, strlen(token) - 1);
    return 1;
}

void grantDatabase(char *username, char *database)
{
    if (strcmp(currentUser, "root") == 0)
    {
        int check = 0;
        char data[2048] = {0};
        sprintf(data, "%s\t%s\n", username, database);
        check = cekDataTersedia(data, "databases/init/permission.table", 69);
        if (check == 0)
        {
            FILE *fptr;
            fptr = fopen("databases/init/permission.table", "a+");
            if (fptr == NULL)
            {
                strcpy(pesan, "Error\n");

                return;
            }
            fprintf(fptr, "%s\t%s\n", username, database);
            memset(data_exist, 0, sizeof(data_exist));
            fclose(fptr);
            strcpy(pesan, "berhasil mazeee\n");
        }
        else
        {
            strcpy(pesan, "the user already has access to the database\n");
            memset(data_exist, 0, sizeof(data_exist));
            return;
        }
    }
    else
    {
        strcpy(pesan, "Sorry, You are not root\n");
    }
}

int cekPerintahRoleDB(char *command, char *username, char *database)
{
    if (strcmp(&command[strlen(command) - 1], ";") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    char *token = strtok(command, " ");
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strcpy(database, token);
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    if (strcmp(token, "INTO") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }

    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strncpy(username, token, strlen(token) - 1);
    return 1;
}

int cekPerintahPakaiDB(char *command, char *database)
{
    if (strcmp(&command[strlen(command) - 1], ";") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    char *token = strtok(command, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strncpy(database, token, strlen(token) - 1);
    return 1;
}
void useDatabase(char *database)
{
    char data[300] = {0};
    sprintf(data, "%s\t%s\n", currentUser, database);
    int check = cekDataTersedia(data, "databases/init/permission.table", 69);
    if (check)
    {
        strcpy(currentDatabase, database);
        strcpy(pesan, "berhasil mazeee\n");
    }
    else
    {
        strcpy(pesan, "Database Not Found / You Not have Permission\n");
    }
    memset(data_exist, 0, sizeof(data_exist));
}
int checkCommandCreateTable(char *input, char *table, char *column_name)
{
    char hasil[2048] = {0};
    if (strcmp(&input[strlen(input) - 1], ";") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    char *token = strtok(input, " ");
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    token = strtok(NULL, " ");
    strcpy(table, token);
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    token = strtok(NULL, "(");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    else if (strcmp(token, ");") == 0)
    {
        strcpy(pesan, "Your Column Is Empty\n");
        return 0;
    }
    else
    {
        strcpy(column, " ");
        strncat(column, token, strlen(token) - 2);
        int j = 0;
        char *token2 = strtok(column, " ");
        j++;
        if (token == NULL)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        strcat(column_name, token2);
        strcat(column_name, "|");
        token2 = strtok(NULL, " ");
        j++;
        if (token == NULL)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        while (token2 != NULL)
        {

            if (strstr(token2, ","))
            {
                strncat(column_name, token2, strlen(token2) - 1);
                strcat(column_name, "\t");
            }
            else
            {
                strcat(column_name, token2);
                strcat(column_name, "|");
            }

            token2 = strtok(NULL, " ");
            j++;
        }
        if (j % 2 == 0)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }

        column_name[strlen(column_name) - 1] = 0;
    }


    return 1;
}
void createTable(char *table, char *column_name)
{
    FILE *fptr;
    char fullpath[500] = {0};
    sprintf(fullpath, "databases/%s/%s", currentDatabase, table);
    fptr = fopen(fullpath, "a+");
    if (fptr == NULL)
    {
        strcpy(pesan, "Error\n");

        return;
    }
    fprintf(fptr, "%s\n", column_name);
    fclose(fptr);
    strcpy(pesan, "berhasil mazeee\n");
}

int cekPerintahDropDB(char *command, char *database)
{

    if (strcmp(&command[strlen(command) - 1], ";") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    char *token = strtok(command, " ");
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strncpy(database, token, strlen(token) - 1);
    return 1;
}
int tester;
void dropDatabase(char *data, char *database)
{   
    tester=0;
    char fullpathCommand[2048] = {0};
    sprintf(fullpathCommand, "rm -r databases/%s", database);
    system(fullpathCommand);
    readDatabase("init", "permission.table");
    writeTable("databases/init/permission.table");
    strcpy(pesan, "berhasil mazeee\n");
}
int checkCommandDropTable(char *command, char *table)
{   
    tester=0;
    if (strcmp(&command[strlen(command) - 1], ";") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    char *token = strtok(command, " ");
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    token = strtok(NULL, " ");
    if (token == NULL)
    {   
        tester=1;
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strncpy(table, token, strlen(token) - 1);
    return 1;
}
int bacaTabelDatabase(char *fullpath, char *table)
{   
    tester=0;
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fullpath)) != NULL)
    {
        tester=0;
        while ((ent = readdir(dir)) != NULL)
        {
            if (strcmp(ent->d_name, table) == 0)
            {
                closedir(dir);
                return 1;
            }
        }
        closedir(dir);
        return 0;
    }
}

void dropTable(char *table)
{   
    tester=0;
    char fullpath[500] = {0};
    sprintf(fullpath, "databases/%s/%s", currentDatabase, table);
    if (remove(fullpath) == 0)
        strcpy(pesan, "berhasil mazeee\n");

    else
        strcpy(pesan, "Error\n");
}
int checkCommandDropColumn(char *command, char *column, char *table)
{
    if (strcmp(&command[strlen(command) - 1], ";") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    char *token = strtok(command, " ");
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strncpy(column, token, strlen(token));
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strncpy(table, token, strlen(token) - 1);
    return 1;
}

int positionCloumn(char *column_name)
{
    char temp[200] = {0};
    strcpy(temp, column);
    int i = 0;
    char *token = strtok(temp, "\t");
    while (token != NULL)
    {
        if (strstr(token, column_name))
        {
            break;
        }
        token = strtok(NULL, "\t");
        tester=1;
        i++;
    }
    return i;
}

void dropColumn(int position)
{
    char temp[200][2048] = {0};
    char hasil[200][2048] = {0};
    for (int i = 0; i < count_row; i++)
    {
        strcpy(temp[i], currData[i]);
    }

    for (int i = 0; i < count_row; i++)
    {

        char *token = strtok(temp[i], "\t");
        int count = 0;
        while (count < count_column)
        {
          
            if (count == position)
            {
                token = strtok(NULL, "\t");
                count++;
                continue;
                tester=1;
            }
            else
            {

                strcat(hasil[i], token);
                if (!strstr(hasil[i], "\n"))
                {
                    strcat(hasil[i], "\t");
                }
                token = strtok(NULL, "\t");
                count++;
            }
        }

        if (!strstr(hasil[i], "\n"))
        {   tester =1;
            strncpy(hasil[i], hasil[i], strlen(hasil[i]) - 1);
            strcat(hasil[i], "\n");
        }
    }
    for (int i = 0; i < count_row; i++)
    {
        strcpy(currData[i], hasil[i]);
    }
    for (int i = 0; i < 100; i++)
    {
        data_exist[i] = -2;
    }
}

void dropColumn2(int position)
{
    char tmp[2048] = {0};
    char hasil[2048] = {0};
    strcpy(tmp, column);
    char *token = strtok(tmp, "\t");
    int count = 0;
    while (count < count_column)
    {
        if (count == position)
        {
            token = strtok(NULL, "\t");
            count++;
            continue;
        }
        else
        {
            strcat(hasil, token);
            if (!strstr(hasil, "\n"))
            {
                strcat(hasil, "\t");
            }
            token = strtok(NULL, "\t");
            count++;
        }
    }

    if (!strstr(hasil, "\n"))
    {
        strncpy(hasil, hasil, strlen(hasil) - 1);
        strcat(hasil, "\n");
    }
    strcpy(column, hasil);
}

int checkCommandInsert(char *input, char *column_name, char *table, int *total)
{
    char hasil[2048] = {0};
    char kolom[2048] = {0};
    int j = 0;
    if (strcmp(&input[strlen(input) - 1], ";") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    char *token = strtok(input, " ");
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    token = strtok(NULL, " ");
    strcpy(table, token);
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    if (strstr(token, "\'"))
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }

    token = strtok(NULL, "(");
    if (strcmp(token, ");") == 0)
    {
        return 0;
    }
    strcpy(kolom, " ");
    strncat(kolom, token, strlen(token) - 2);
    char *token2 = strtok(kolom, " ");
    j++;
    while (token2 != NULL)
    {
        if (strstr(token2, ","))
        {
            if (strstr(token2, "\'"))
            {
                char tmp[200] = {0};
                strcpy(tmp, token2);
                memmove(tmp, tmp + 1, strlen(tmp));
                strncat(column_name, tmp, strlen(tmp) - 2);
                strcat(column_name, "\t");
            }
            else
            {
                strncat(column_name, token2, strlen(token2) - 1);
                strcat(column_name, "\t");
            }
        }
        else
        {
            if (strstr(token2, "\'"))
            {
                char tmp[200] = {0};
                strcpy(tmp, token2);
                memmove(tmp, tmp + 1, strlen(tmp));
                strncat(column_name, tmp, strlen(tmp) - 1);
            }
            else
            {
                strncat(column_name, token2, strlen(token2));
            }
        }
        token2 = strtok(NULL, " ");
        j++;
    }
    *total = j;
    if (strstr(column_name, ","))
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    return 1;
}

void insert(char *data, char *table)
{
    FILE *fptr;
    char fullpath[200] = {0};
    sprintf(fullpath, "databases/%s/%s", currentDatabase, table);
    fptr = fopen(fullpath, "a+");
    if (fptr == NULL)
    {
        strcpy(pesan, "Error\n");

        return;
    }
    fprintf(fptr, "%s\n", data);
    fclose(fptr);
    strcpy(pesan, "berhasil mazeee\n");
}

int checkCommandDelete(char *input, char *table, char *column_name, char *data_column, int *mode)
{
    int j = 0;
    if (strcmp(&input[strlen(input) - 1], ";") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    char *token = strtok(input, " ");
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strcpy(table, token);

    token = strtok(NULL, " ");
    if (token == NULL)
    {
        table[strlen(table) - 1] = '\0';
        *mode = 1;
    }
    else
    {
        *mode = 2;
        token = strtok(NULL, " ");
        if (token == NULL)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        char tmp[200] = {0};
        strcpy(tmp, token);
        char *token2 = strtok(tmp, "=");
        if (token2 == NULL)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        strcpy(column_name, token2);
        token2 = strtok(NULL, "=");
        if (strstr(token2, "\'"))
        {
            if (token2[0] == '\'')
            {
                memmove(token2, token2 + 1, strlen(token2));
            }

            strncpy(data_column, token2, strlen(token2) - 2);
        }
        else
        {
            strncpy(data_column, token2, strlen(token2) - 1);
        }
    }

    return 1;
}
long debug;
void deleteData(int mode, int position, char *fullpath)
{
    debug=0;
    if (mode == 1)
    {
        debug=0;
        FILE *fptr;
        fptr = fopen(fullpath, "w+");
        if (fptr == NULL)
        {
            strcpy(pesan, "Error\n");
            return;
        }
        fprintf(fptr, "%s", column);
        strcpy(pesan, "berhasil mazeee\n");
        fclose(fptr);
    }
    else if (mode == 2)
    {
        debug=1;
        FILE *fptr;
        int i = -1, j = 0;
        fptr = fopen(fullpath, "w+");
        if (fptr == NULL)
        {
            strcpy(pesan, "Error\n");
            return;
        }
        for (int i = -1; i < count_row; i++)
        {
            if (i == -1)
            {
                fprintf(fptr, "%s", column);
            }
            else
            {

                if (i == data_exist[j])
                {
                    j++;
                    continue;
                }
                fprintf(fptr, "%s", currData[i]);
            }
        }
        strcpy(pesan, "berhasil mazeee\n");
        fclose(fptr);
    }
}

int cekPerintahUpdate(char *input, char *table, char *data_column, char *column_name, char *search_column, char *search_data, int *mode)
{

    int j = 0;
    debug=0;
    if (strcmp(&input[strlen(input) - 1], ";") != 0)
    {
        debug=0;
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }

    char *token = strtok(input, " ");

    token = strtok(NULL, " ");
    if (token == NULL)
    {
        debug=0;
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strcpy(table, token);

    token = strtok(NULL, " ");

    if (token == NULL)
    {
        debug=0;
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }

    token = strtok(NULL, " ");
    char temp[2048] = {0};
    char temp2[2048] = {0};
    strcpy(temp, token);
    token = strtok(NULL, " ");
    //////printf("5 %s\n", token);
    if (token == NULL)
    {
        debug=0;
        *mode = 1;
        char *token2 = strtok(temp, "=");
        if (token2 == NULL)
        {
            debug=1;
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        strcpy(column_name, token2);
        token2 = strtok(NULL, "=");
        if (strstr(token2, "\'"))
        {
            debug=0;
            if (token2[0] == '\'')
            {
                debug=1;
                memmove(token2, token2 + 1, strlen(token2));
            }

            strncpy(data_column, token2, strlen(token2) - 2);
        }
        else
        {
            strncpy(data_column, token2, strlen(token2) - 1);
        }
        return 1;
    }
    *mode = 2;
    token = strtok(NULL, " ");
    strcpy(temp2, token);
    char *token2 = strtok(temp, "=");
    if (token2 == NULL)
    {
        debug=0;
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strcpy(column_name, token2);
    token2 = strtok(NULL, "=");
    if (strstr(token2, "\'"))
    {
        if (token2[0] == '\'')
        {
            debug=0;
            memmove(token2, token2 + 1, strlen(token2));
        }

        strncpy(data_column, token2, strlen(token2) - 1);
    }
    else
    {
        debug=1;
        strncpy(data_column, token2, strlen(token2));
    }
    char tmp[200] = {0};
    strcpy(tmp, token);
    char *token3 = strtok(tmp, "=");
    if (token3 == NULL)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    strcpy(search_column, token3);
    token3 = strtok(NULL, "=");
    if (strstr(token3, "\'"))
    {
        debug=0;
        if (token3[0] == '\'')
        {
            debug=1;
            memmove(token3, token3 + 1, strlen(token3));
        }

        strncpy(search_data, token3, strlen(token3) - 2);
    }
    else
    {
        strncpy(search_data, token3, strlen(token3) - 1);
    }

    return 1;
}
 
void updateData(int mode, int position, char *data_column, char *search_column, char *search_data)
{
  
    if (mode == 1)
    {
        char tmp[200][2048] = {0};
        char hasil[200][2048] = {0};
        for (int i = 0; i < count_row; i++)
        {
            strncpy(tmp[i], currData[i], strlen(currData[i]) - 1);
        }
        for (int i = 0; i < count_row; i++)
        {
            char *token = strtok(tmp[i], "\t");
            int j = 0;

            while (token != NULL)
            {

                if (j == position)
                {
                    strcat(hasil[i], data_column);
                    strcat(hasil[i], "\t");
                }
                else
                {
                    strcat(hasil[i], token);
                    strcat(hasil[i], "\t");
                }
                token = strtok(NULL, "\t");
                j++;
            }
            hasil[i][strlen(hasil[i]) - 1] = '\0';
            if (!strstr(hasil[i], "\n"))
            {
                strcat(hasil[i], "\n");

            }
        }
        debug=5;
        for (int i = 0; i < count_row; i++)
        {
            strcpy(currData[i], hasil[i]);

        }
        strcpy(pesan, "berhasil mazeee\n");
    }
    else if (mode == 2)
    {
        debug=1;
        char tmp[200][2048] = {0};
        char hasil[200][2048] = {0};
        for (int i = 0; i < count_row; i++)
        {
            strncpy(tmp[i], currData[i], strlen(currData[i]));

        }
        int j = 0;
        for (int i = 0; i < count_row; i++)
        {
            if (i == data_exist[j])
            {
                char *token = strtok(tmp[i], "\t");
                int k = 0;

                while (token != NULL)
                {
                    if (k == position)
                    {
                        strcat(hasil[i], data_column);
                        strcat(hasil[i], "\t");
                    }
                    else
                    {
                        strcat(hasil[i], token);
                        strcat(hasil[i], "\t");
                    }
                    token = strtok(NULL, "\t");
                    k++;
                }
                hasil[i][strlen(hasil[i]) - 1] = '\0';

                j++;
            }
            else
            {

                strcat(hasil[i], tmp[i]);
            }
            if (!strstr(hasil[i], "\n"))
            {
                strcat(hasil[i], "\n");

            }
        }
        for (int i = 0; i < count_row; i++)
        {
            strcpy(currData[i], hasil[i]);

        }
        strcpy(pesan, "berhasil mazeee\n");
    }
}
int cekPerintah(char *input, char *table, char *search_column, char *search_data, int *mode, int *countColumnSelect)
{
    int j = 0;

    if (strcmp(&input[strlen(input) - 1], ";") != 0)
    {
        strcpy(pesan, "salah sintax mazeee\n");

        return 0;
    }
    if (strstr(input, "*"))
    {

        char *token = strtok(input, " ");

        token = strtok(NULL, " ");
        if (token == NULL)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        if (strcmp(token, "*") != 0)
        {
            return 0;
        }
        token = strtok(NULL, " ");
        if (token == NULL)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        token = strtok(NULL, " ");
        if (token == NULL)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        if (strstr(token, ";"))
        {
            strncpy(table, token, strlen(token) - 1);
        }
        else
        {
            strncpy(table, token, strlen(token));
        }

        token = strtok(NULL, " ");
        if (token == NULL)
        {
            *mode = 1;
            return 1;
        }
        if (strcmp(token, "WHERE") == 0)
        {
            token = strtok(NULL, " ");
            char tmp[200] = {0};
            strcpy(tmp, token);
            char *token2 = strtok(tmp, "=");
            strcpy(search_column, token2);
            token2 = strtok(NULL, " ");

            strcpy(search_data, token2);
            if (search_data[0] == '\'')
            {
                memmove(search_data, search_data + 1, strlen(search_data));
                search_data[strlen(search_data) - 1] = '\0';
                search_data[strlen(search_data) - 1] = '\0';
            }
            else
            {
                search_data[strlen(search_data) - 1] = '\0';
            }
        }
        else
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        *mode = 2;
        return 1;
    }
    else
    {

        char *token = strtok(input, " ");

        if (token == NULL)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        token = strtok(NULL, " ");
        if (strcmp(token, "FROM") == 0)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        int i = 0;
        while (strcmp(token, "FROM") != 0)
        {

            strcat(currColName[i], token);
            if (strstr(currColName[i], ","))
            {
                currColName[i][strlen(currColName[i]) - 1] = '\0';
            }

            token = strtok(NULL, " ");
            *countColumnSelect = *countColumnSelect + 1;
            i++;
        }

        if (token == NULL)
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }

        token = strtok(NULL, " ");

        if (strstr(token, ";"))
        {
            strncpy(table, token, strlen(token) - 1);
        }
        else
        {
            strncpy(table, token, strlen(token));
        }

        token = strtok(NULL, " ");
        if (token == NULL)
        {
            *mode = 3;
            return 1;
        }
     
        if (strcmp(token, "WHERE") == 0)
        {
            token = strtok(NULL, " ");

            char tmp[200] = {0};
            strcpy(tmp, token);
            char *token2 = strtok(tmp, "=");
            strcpy(search_column, token2);
            token2 = strtok(NULL, " ");

            strcpy(search_data, token2);
            if (search_data[0] == '\'')
            {
                memmove(search_data, search_data + 1, strlen(search_data));
                search_data[strlen(search_data) - 1] = '\0';
                search_data[strlen(search_data) - 1] = '\0';
            }
            else
            {
                search_data[strlen(search_data) - 1] = '\0';
            }

        }
        else
        {
            strcpy(pesan, "salah sintax mazeee\n");

            return 0;
        }
        *mode = 4;
        return 1;
    }
}

void dropColumnForSelect(char *column_name, int position)
{
    char temp[200][2048] = {0};
    char hasil[200][2048] = {0};
    for (int i = 0; i < count_row; i++)
    {
        debug=0;
        strcpy(temp[i], currData[i]);
    }

    for (int i = 0; i < count_row; i++)
    {
        debug=1;
        char *token = strtok(temp[i], "\t");
        int count = 0;
        while (count < count_column)
        {
            if (count == position)
            {
                debug=0;
                token = strtok(NULL, "\t");
                count++;
                continue;
            }
            else
            {
                debug=1;
                strcat(hasil[i], token);
                if (!strstr(hasil[i], "\n"))
                {
                    strcat(hasil[i], "\t");
                }
                token = strtok(NULL, "\t");
                count++;
            }
        }

        if (!strstr(hasil[i], "\n"))
        {
            strncpy(hasil[i], hasil[i], strlen(hasil[i]) - 1);
            strcat(hasil[i], "\n");
        }

    }
    for (int i = 0; i < count_row; i++)
    {
        strcpy(currData[i], hasil[i]);
    }
    for (int i = 0; i < 100; i++)
    {
        data_exist[i] = -2;
    }
}

void AddAjalah(int mode)
{
    debug=0;
    char output[2048] = {0};
    int j = 0;
    int l = 0;
    char tmp[200][2048] = {0};
    char tmpcolumn[2048] = {0};
    char hasilcolumn[2048] = {0};
    char hasil[200][2048] = {0};
    for (int i = 0; i < count_row; i++)
    {
        debug=0;
        strcpy(tmp[i], currData[i]);
    }
    for (int i = -1; i < count_row; i++)
    {
        debug=0;
        if (i == -1)
        {
            debug=0;
            if (mode == 3 || mode == 4)
            {
                strcpy(tmpcolumn, column);
                char *token = strtok(tmpcolumn, "\t");
                int j = 0;
                int k = 0;
                while (token != NULL)
                {
                    if (j != position_column[k])
                    {
                        debug=0;
                        j++;
                        token = strtok(NULL, "\t");
                        continue;
                    }
                    else
                    {
                        debug=1;
                        strcat(hasilcolumn, token);
                        if (!strstr(hasilcolumn, "\n"))
                        {
                            strcat(hasilcolumn, "\t");
                        }
                        j++;
                        k++;
                        token = strtok(NULL, "\t");
                    }
                }
                if (!strstr(hasilcolumn, "\n"))
                {
                    strcat(hasilcolumn, "\n");
                }

                strcat(output, hasilcolumn);
            }
            else
            {
                strcat(output, column);
            }
        }
        else
        {
            if (mode == 1)
            {
                strcat(output, currData[i]);
            }
            if (mode == 2)
            {
                if (i == data_exist[j])
                {
                    strcat(output, currData[i]);
                    j++;
                }
                else
                {
                    continue;
                }
            }
            if (mode == 3)
            {
                char *token = strtok(tmp[i], "\t");
                int j = 0;
                int k = 0;
                while (token != NULL)
                {
                    if (j != position_column[k])
                    {
                        j++;
                        token = strtok(NULL, "\t");
                        continue;
                    }
                    else
                    {
                        strcat(hasil[i], token);
                        if (!strstr(hasil[i], "\n"))
                        {
                            strcat(hasil[i], "\t");
                        }
                        j++;
                        k++;
                        token = strtok(NULL, "\t");
                    }
                }
                if (!strstr(hasil[i], "\n"))
                {
                    strcat(hasil[i], "\n");
                }
                strcat(output, hasil[i]);
            }
            if (mode == 4)
            {
                char *token = strtok(tmp[i], "\t");
                int j = 0;
                int k = 0;

                while (token != NULL)
                {
                    if (j != position_column[k])
                    {
                        j++;
                        token = strtok(NULL, "\t");
                        continue;
                    }
                    else
                    {
                        strcat(hasil[i], token);
                        if (!strstr(hasil[i], "\n"))
                        {
                            strcat(hasil[i], "\t");
                        }
                        j++;
                        k++;
                        token = strtok(NULL, "\t");
                    }
                }
                if (!strstr(hasil[i], "\n"))
                {
                    strcat(hasil[i], "\n");
                }
                if (i == data_exist[l])
                {
                    strcat(output, hasil[i]);
                    l++;
                }
                else
                {
                    continue;
                }
            }
        }
    }
    strcpy(pesan, output);
}

void selectCommand(int mode)
{
    if (mode == 1)
    {
        debug=0;
        AddAjalah(mode);
    }
    else if (mode == 2)
    {
        debug=1;
        AddAjalah(mode);
    }
    else if (mode == 3)
    {
        debug=1;
        AddAjalah(mode);
    }
    else if (mode == 4)
    {
        debug=1;
        AddAjalah(mode);
    }
}

void masukMas(char *input)
{

    FILE *f = fopen("database.log", "a+");
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    fprintf(f, "%04d-%02d-%02d %02d:%02d:%02d:%s:%s\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, currentUser, input);
    fclose(f);
}

void tuker(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void Sorting(int arr[], int n)
{
    int i, j, min_idx;

    for (i = 0; i < n - 1; i++)
    {

        min_idx = i;
        for (j = i + 1; j < n; j++)
            if (arr[j] < arr[min_idx])
                min_idx = j;


        tuker(&arr[min_idx], &arr[i]);
    }
}
int debugging;
int checkDatabase()
{
    struct dirent *de;

    DIR *dr = opendir(".");

    if (dr == NULL)
    {
        strcpy(pesan, "ngga bisa buka folder mazeee\n");
        return 0;
    }
    while ((de = readdir(dr)) != NULL)
    {
        if (strcmp(de->d_name, "databases") == 0)
        {
            return 1;
        }
    }
    debugging=1;
    closedir(dr);
    return 0;
}

int main(int argc, char const *argv[])
{
    
    char sendConnection[2048] = {0};
    char acceptConnection[2048] = {0};
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[2048] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    debugging=1;
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {   
        debugging=0;
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    while (1)
    {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
        {
            perror("accept");
            exit(EXIT_FAILURE);
        }
        debugging=1;

        umask(0);
        
        int testing = checkDatabase();
        if (!testing)
        {
            initDatabase();
        }

        int verifikasi = 0;
        char username[200] = {0};
        char password[200] = {0};

        while (verifikasi == 0)
        {
            debugging=1;

            read(new_socket, acceptConnection, 2048);
            char *token = strtok(acceptConnection, "\t");
            strcpy(username, token);
            token = strtok(NULL, "\t");
            strcpy(password, token);
            verifikasi = checkUser(username, password);

            if (verifikasi)
            {
                debugging=1;
                strcpy(sendConnection, "bisa login mazeee\n");
                send(new_socket, sendConnection, strlen(sendConnection), 0);
            }

            else
            {
                debugging=1;
                strcpy(sendConnection, "gabisa login mazeee\n");
                send(new_socket, sendConnection, strlen(sendConnection), 0);
                break;
                
            }

            memset(sendConnection, 0, 2048);
            memset(acceptConnection, 0, 2048);
        }
        while (verifikasi)
        {
            strcpy(pesan,"Input Your Command\n");
            read(new_socket, acceptConnection, 2048);
            char input[2048];
            strcpy(input, acceptConnection);
            debugging=0;
            input[strcspn(input, "\n")] = 0;
            masukMas(input);
            if (strncmp(input, "CREATE USER", 11) == 0)
            {
                char username[200] = {0};
                char password[200] = {0};
                if (checkCommandAddUser(input, username, password))
                {
                    addUser(username, password);
                }
            }
            if (strncmp(input, "CREATE DATABASE", 15) == 0)
            {
                char database[200] = {0};
                if (cekPerintahBuatDB(input, database))
                {
                    createDatabase(database);
                }
            }
            if (strncmp(input, "GRANT PERMISSION", 16) == 0)
            {
                char username[200] = {0};
                char database[200] = {0};
                if (cekPerintahRoleDB(input, username, database))
                {
                    grantDatabase(username, database);
                }
            }
            if (strncmp(input, "USE", 3) == 0)
            {
                char username[200] = {0};
                char database[200] = {0};
                if (cekPerintahPakaiDB(input, database))
                {
                    debugging=1;
                    useDatabase(database);
                }
            }
            if (strncmp(input, "CREATE TABLE", 12) == 0)
            {
                char username[200] = {0};
                char column_name[200] = {0};
                char table[200] = {0};
                if (strcmp(currentDatabase, "") != 0)
                {
                        debugging=0;
                    if (checkCommandCreateTable(input, table, column_name))
                    {
                        debugging=1;
                        char fullpath[500] = {0};
                        sprintf(fullpath, "databases/%s", currentDatabase);
                        int check = bacaTabelDatabase(fullpath, table);
                        if (check == 0)
                        {
                            debugging=1;
                            createTable(table, column_name);
                        }
                        else
                        {
                            debugging=1;
                            strcpy(pesan, "Table Already Used\n");
                        }
                    }
                }
                else
                {
                    strcpy(pesan, "sebutkan spesifikasi database mazeee\n");
                }
            }
            if (strncmp(input, "DROP DATABASE", 13) == 0)
            {
                debugging=1;
                char username[200] = {0};
                char database[200] = {0};
                if (cekPerintahDropDB(input, database))
                {
                    char data[2048] = {0};
                    sprintf(data, "%s\t%s\n", currentUser, database);
                    strcat(database, "\n");
                    int check = cekDataTersedia(database, "databases/init/permission.table", 1);
                    if (check)
                    {
                        strtok(database, "\n");
                        dropDatabase(data, database);
                    }
                    else
                    {
                        strcpy(pesan, "Database Not Found / You Not have Permission\n");
                    }
                }
            }
            if (strncmp(input, "DROP TABLE", 10) == 0)
            {
                int check = 0;
                char table[200] = {0};
                check = checkCommandDropTable(input, table);
                if (check)
                {
                    if (strcmp(currentDatabase, "") != 0)
                    {
                        char fullpath[200];
                        sprintf(fullpath, "databases/%s", currentDatabase);
                        if (bacaTabelDatabase(fullpath, table))
                        {
                            dropTable(table);
                        }
                        else
                        {
                            strcpy(pesan, "tabel kosong mazeee\n");
                        }
                    }
                    else
                    {
                        debugging=1;
                        strcpy(pesan, "sebutkan spesifikasi database mazeee\n");
                    }
                }
            }
            if (strncmp(input, "DROP COLUMN", 11) == 0)
            {
                debugging=1;
                int check = 0;
                char column_name[200] = {0};
                char table[200] = {0};
                check = checkCommandDropColumn(input, column_name, table);
                if (check)
                {
                    if (strcmp(currentDatabase, "") != 0)
                    {

                        debugging=1;
                        readDatabase(currentDatabase, table);
                        countColDatabase();
                      
                        char fullpath[400] = {0};
                        sprintf(fullpath, "databases/%s", currentDatabase);
                        if (strstr(column, column_name))
                        {
                            
                            if (bacaTabelDatabase(fullpath, table))
                            {

                                int position = positionCloumn(column_name);
                                dropColumn(position);
                                dropColumn2(position);
                                sprintf(fullpath, "databases/%s/%s", currentDatabase, table);
                                writeTable(fullpath);
                                strcpy(pesan, "berhasil mazeee\n");
                            }
                            else
                            {
                                strcpy(pesan, "tabel kosong mazeee\n");
                            }
                        }
                        else
                        {
                            strcpy(pesan, "kolom kosong mazeee\n");
                        }
                    }
                    else
                    {
                        strcpy(pesan, "sebutkan spesifikasi database mazeee\n");
                    }
                }
            }
            if (strncmp(input, "INSERT INTO", 11) == 0)
            {

                char column_name[200] = {0};
                char table[200] = {0};
                int total = 0;
                int check = checkCommandInsert(input, column_name, table, &total);
                char fullpath[200] = {0};
                sprintf(fullpath, "databases/%s", currentDatabase);
                if (strcmp(currentDatabase, "") != 0)
                {
                    char fullpath[200] = {0};
                    sprintf(fullpath, "databases/%s", currentDatabase);
                    if (bacaTabelDatabase(fullpath, table))
                    {
                        if (check)
                        {
                            readDatabase(currentDatabase, table);
                            countColDatabase();
                            if (count_column == total - 1)
                            {

                                insert(column_name, table);
                            }
                            else
                            {
                                strcpy(pesan, "too many or too few columns\n");
                            }
                        }
                    }
                    else
                    {
                        strcpy(pesan, "tabel kosong mazeee\n");
                    }
                }
                else
                {
                    strcpy(pesan, "sebutkan spesifikasi database mazeee\n");
                }
            }
            if (strncmp(input, "DELETE FROM", 11) == 0)
            {
                char table[200] = {0};
                char column_name[200] = {0};
                char data_column[200] = {0};
                
                int mode;

                if (strcmp(currentDatabase, "") != 0)
                {
                    int check = checkCommandDelete(input, table, column_name, data_column, &mode);
                    if (check)
                    {
                        char fullpath[400] = {0};
                        sprintf(fullpath, "databases/%s/%s", currentDatabase, table);

                        char fullpath2[400] = {0};
                        sprintf(fullpath2, "databases/%s", currentDatabase);
                        readDatabase(currentDatabase, table);
                        countColDatabase();
                        int position = positionCloumn(column_name);
                      
                        if (strstr(column, column_name))
                        {
                            if (bacaTabelDatabase(fullpath2, table))
                            {
                                if (count_column == position + 1)
                                {
                                    strcat(data_column, "\n");
                                }
                                if (mode == 2)
                                {
                                    cekDataTersedia(data_column, fullpath, position);
                                   
                                    if (count_data_exist != 0)
                                    {
                                        deleteData(mode, position, fullpath);
                                    }
                                    else
                                    {
                                        strcpy(pesan, "data tidak tersedia mazeee\n");
                                    }
                                }
                                else
                                {
                                    deleteData(mode, position, fullpath);
                                }
                            }
                            else
                            {
                                strcpy(pesan, "tabel kosong mazeee\n");
                            }
                        }
                        else
                        {
                            strcpy(pesan, "kolom kosong mazeee\n");
                        }
                    }
                }
                else
                {
                    strcpy(pesan, "sebutkan spesifikasi database mazeee\n");
                }
            }

            if (strncmp(input, "UPDATE", 6) == 0)
            {
                char column_name[200] = {0};
                char data_column[200] = {0};
                char search_column[200] = {0};
                char search_data[200] = {0};
                int mode;
                char table[200] = {0};
                int total = 0;

                if (strcmp(currentDatabase, "") != 0)
                {
                    int check = cekPerintahUpdate(input, table, data_column, column_name, search_column, search_data, &mode);
                    if (check)
                    {
                        readDatabase(currentDatabase, table);
                        countColDatabase();
                        if (strstr(column, column_name))
                        {
                            char fullpath[200] = {0};
                            sprintf(fullpath, "databases/%s", currentDatabase);
                            if (bacaTabelDatabase(fullpath, table))
                            {

                                int position = positionCloumn(column_name);
                                int position_search = positionCloumn(search_column);
                                char fullpath[400] = {0};
                                sprintf(fullpath, "databases/%s/%s", currentDatabase, table);
                                if (position_search + 1 == count_column)
                                {
                                    strcat(search_data, "\n");
                                }

                                cekDataTersedia(search_data, fullpath, position_search);
                                updateData(mode, position, data_column, search_column, search_data);
                                memset(data_exist, -2, sizeof(data_exist));
                                writeTable(fullpath);
                            }
                            else
                            {
                                strcpy(pesan, "tabel kosong mazeee\n");
                            }
                        }
                        else
                        {
                            strcpy(pesan, "kolom kosong mazeee\n");
                        }
                    }
                }
                else
                {
                    strcpy(pesan, "sebutkan spesifikasi database mazeee\n");
                }
            }
            if (strncmp(input, "SELECT", 6) == 0)
            {
                char search_column[200] = {0};
                char search_data[200] = {0};
                int mode;
                int countColumnSelect = 0;
                char table[200] = {0};
                if (strcmp(currentDatabase, "") != 0)
                {
                    int check = cekPerintah(input, table, search_column, search_data, &mode, &countColumnSelect);
                    if (check)
                    {
                        readDatabase(currentDatabase, table);
                        countColDatabase();
                        int check = 1;
                        for (int i = 0; i < countColumnSelect; i++)
                        {
                            if (!strstr(column, currColName[i]))
                            {
                                check = 0;
                            }
                        }
                        if (check)
                        {
                            int check2 = 1;
                            if (mode == 2 || mode == 4)
                            {
                                if (!strstr(column, search_column))
                                {
                                    check2 = 0;
                                }
                            }
                            if (check2)
                            {
                                char fullpath[200] = {0};
                                sprintf(fullpath, "databases/%s", currentDatabase);
                              
                                if (bacaTabelDatabase(fullpath, table))
                                {
                                   
                                    int position_search = positionCloumn(search_column);

                                    for (int i = 0; i < countColumnSelect; i++)
                                    {
                                        position_column[i] = positionCloumn(currColName[i]);
                                      
                                    }

                                    Sorting(position_column, countColumnSelect);
                                    for (int i = 0; i < countColumnSelect; i++)
                                    {
                                        
                                    }
                                    char fullpath[400] = {0};

                                    sprintf(fullpath, "databases/%s/%s", currentDatabase, table);
                                    if (position_search + 1 == count_column)
                                    {
                                        strcat(search_data, "\n");
                                    }

                                    cekDataTersedia(search_data, fullpath, position_search);
                                    selectCommand(mode);
                                }
                                else
                                {
                                    strcpy(pesan, "tabel kosong mazeee\n");
                                }
                            }
                            else
                            {
                                strcpy(pesan, "kolom kosong mazeee\n");
                            }
                        }

                        else
                        {
                            strcpy(pesan, "kolom kosong mazeee\n");
                        }
                    }
                }
                else
                {
                    strcpy(pesan, "sebutkan spesifikasi database mazeee\n");
                }
            }
            if (strncmp(input, "exit", 4) == 0)
            {
                reset();
                strcpy(sendConnection, "Watashi keluar\n");
                send(new_socket, sendConnection, strlen(sendConnection), 0);
                strcpy(pesan, "");
                strcpy(currentDatabase, "");
                strcpy(currentUser, "");
                memset(sendConnection, 0, 2048);
                memset(acceptConnection, 0, 2048);
                break;
            }
            int testcoba;
            if (strncmp(input, "DUMP", 4) == 0)
            {
                testcoba=0;
                char database[200] = {0};
                char *token = strtok(input, " ");
                token = strtok(NULL, " ");
                strcpy(database, token);
                useDatabase(database);

                if (strcmp(token, "*") == 0)
                {
                }
                else
                {
                    testcoba=0;
                    if (strcmp(currentDatabase, "") != 0)
                    {
                        strcpy(pesan, "");
                        struct dirent *de;
                        char path[200] = {0};
                        sprintf(path, "databases/%s", currentDatabase);

                        DIR *dr = opendir(path);
             
                        while ((de = readdir(dr)) != NULL)
                        {
                            char verifikasiKolom[200] = {0};

                            int tipedata[200] = {0};

                            if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
                            {
                                
                                continue;
                            }
                            else
                            {
                                testcoba=1;
                                char table[200] = {0};
                                strcpy(table, de->d_name);
                                readDatabase(currentDatabase, table);
                                char strcolumn[200] = {0};
                                strcpy(strcolumn, column);
                                strtok(strcolumn, "\n");
                                char *token = strtok(strcolumn, "\t");

                                int i = 0;
                                while (token != NULL)
                                {
                                    testcoba=0;
                                    char tmp[2048] = {0};
                                    strcpy(tmp, token);
                                    if (strstr(token, "string"))
                                    {
                                        tipedata[i] = 0;
                                        strncat(verifikasiKolom, tmp, strlen(tmp) - 7);
                                        strcat(verifikasiKolom, " ");
                                        strcat(verifikasiKolom, "string");
                                        strcat(verifikasiKolom, ",");
                                        strcat(verifikasiKolom, " ");
                                    }
                                    else
                                    {
                                        testcoba=1;
                                        tipedata[i] = 1;
                                        strncat(verifikasiKolom, tmp, strlen(tmp) - 4);
                                        strcat(verifikasiKolom, " ");
                                        strcat(verifikasiKolom, "int");
                                        strcat(verifikasiKolom, ",");
                                        strcat(verifikasiKolom, " ");
                                    }
                                    token = strtok(NULL, "\t");
                                    i++;
                                }

                                verifikasiKolom[strlen(verifikasiKolom) - 2] = '\0';
                                char drop[200] = {0};
                                char create[120] = {0};
                                sprintf(drop, "DROP TABLE %s;\n", table);
                                sprintf(create, "CREATE TABLE %s (%s);\n\n", table, verifikasiKolom);
                                strcat(pesan, drop);
                                strcat(pesan, create);
                              
                                char temp[200][2048] = {0};
                                for (int i = 0; i < count_row; i++)
                                {
                                    testcoba=2;
                                    strcpy(temp[i], currData[i]);
                                  
                                }

                                for (int i = 0; i < count_row; i++)
                                {
                                    char dataVerifikasi[2048] = {0};
                                    strtok(temp[i], "\n");
                                    char *token2 = strtok(temp[i], "\t");
                                  

                                    int j = 0;
                                    while (token2 != NULL)
                                    {
                                        testcoba=1;
                                        char tmp[200] = {0};
                                        strcpy(tmp, token2);
                                        if (tipedata[j] == 0)
                                        {
                                            testcoba=1;
                                            strcat(dataVerifikasi, "'");
                                            strcat(dataVerifikasi, tmp);
                                            strcat(dataVerifikasi, "'");
                                            strcat(dataVerifikasi, ",");
                                            strcat(dataVerifikasi, " ");
                                        }
                                        else
                                        {
                                            strcat(dataVerifikasi, tmp);
                                            strcat(dataVerifikasi, ",");
                                            strcat(dataVerifikasi, " ");
                                        }
                                        token2 = strtok(NULL, "\t");
                                        j++;
                                    }
                                    dataVerifikasi[strlen(dataVerifikasi) - 2] = '\0';
                                 
                                    char insert[2048] = {0};

                                    sprintf(insert, "INSERT INTO %s (%s);\n", table, dataVerifikasi);
                                    strcat(pesan, insert);
                                }
                            }

                            strcat(pesan,"\n");
                        }
                        closedir(dr);
                    }
                    reset();
                    strcpy(sendConnection, pesan);
                    send(new_socket, sendConnection, strlen(sendConnection), 0);
                    strcpy(pesan, "");
                    strcpy(currentDatabase, "");
                    strcpy(currentUser, "");
                    memset(sendConnection, 0, 2048);
                    memset(acceptConnection, 0, 2048);
                    break;
                }
                
            }
            reset();
            strcpy(sendConnection, pesan);
            send(new_socket, sendConnection, strlen(sendConnection), 0);
            strcpy(pesan, "");
            memset(sendConnection, 0, 2048);
            memset(acceptConnection, 0, 2048);
        }
    }

    return 0;
}