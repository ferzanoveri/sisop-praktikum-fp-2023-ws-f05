FROM gcc:latest

ADD client/client.c /app/
ADD database/database.c /app/

RUN gcc app/database.c -o app/database
RUN gcc app/client.c -o app/client