# sisop-praktikum-modul-4-2023-WS-F05

| NAMA  | NRP |
| ------------- | ------------- |
| Azhar Abiyu Rasendriya Harun  | 5025211177  |
| Beauty Valen Fajri  | 5025211227 |
| Ferza Noveri  | 5025211097 |

## Point A
```shell
if (!getuid() == 0)
    {
        if (argc != 5)
        {
            printf("<USAGE>\n./[program_client_database] -u [username] -p [password]\n");
            return 1;
        }
        if ((strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0))
        {
            printf("<USAGE>\n./[program_client_database] -u [username] -p [password]\n");
            return 1;
        }
    }
```
Bagian ini merupakan pengondisian yang memeriksa apakah program dijalankan dengan hak akses root atau bukan. Jika program dijalankan sebagai root, maka akan memeriksa apakah jumlah argumen yang diberikan sesuai dengan format yang diharapkan yaitu ./[program_client_database] -u [username] -p [password]. Jika tidak sesuai, maka akan mencetak pesan kesalahan dan mengembalikan nilai 1. Jika argumen sesuai, maka akan dilakukan pembandingan terhadap argumen untuk memastikan format yang benar.
```shell
void addUser(char *namaUser, char *passwordUser)
{
    if (strcmp(currentUser, "root") == 0)
    {
        int check = cekDataTersedia(namaUser, "databases/init/users.table", 0);
        if (!check)
        {
            FILE *fptr;
            fptr = fopen("databases/init/users.table", "a+");

            if (fptr == NULL)
            {
                strcpy(pesan, "Error\n");

                return;
            }

            fprintf(fptr, "%s\t%s\n", namaUser, passwordUser);
            fclose(fptr);
        }
        else
        {
            strcpy(pesan, "User wes onok mazeee\n");
        }
    }
    else
    {
        strcpy(pesan, "You Are Not root\n");
    }
    strcpy(pesan, "berhasil mazeee\n");
    memset(data_exist, 0, sizeof(data_exist));
}
```
Ketika kita menjalankan perintah `CREATE USER` Akan memanggil fungsi di atas.

## Point B
```shell
void useDatabase(char *database)
{
    char data[300] = {0};
    sprintf(data, "%s\t%s\n", currentUser, database);
    int check = cekDataTersedia(data, "databases/init/permission.table", 69);
    if (check)
    {
        strcpy(currentDatabase, database);
        strcpy(pesan, "berhasil mazeee\n");
    }
    else
    {
        strcpy(pesan, "Database Not Found / You Not have Permission\n");
    }
    memset(data_exist, 0, sizeof(data_exist));
}
```
Ketika menggunakan command `USE` memanggil fungsi di atas.
```shell
void grantDatabase(char *username, char *database)
{
    if (strcmp(currentUser, "root") == 0)
    {
        int check = 0;
        char data[2048] = {0};
        sprintf(data, "%s\t%s\n", username, database);
        check = cekDataTersedia(data, "databases/init/permission.table", 69);
        if (check == 0)
        {
            FILE *fptr;
            fptr = fopen("databases/init/permission.table", "a+");
            if (fptr == NULL)
            {
                strcpy(pesan, "Error\n");

                return;
            }
            fprintf(fptr, "%s\t%s\n", username, database);
            memset(data_exist, 0, sizeof(data_exist));
            fclose(fptr);
            strcpy(pesan, "berhasil mazeee\n");
        }
        else
        {
            strcpy(pesan, "the user already has access to the database\n");
            memset(data_exist, 0, sizeof(data_exist));
            return;
        }
    }
    else
    {
        strcpy(pesan, "Sorry, You are not root\n");
    }
}
```
Ketika menggunakan command `GRANT PERMISSION` akan memanggil fungsi di atas, yang mana hanya bisa dilakukan oleh user root.

## Point C
```shell
void createDatabase(char *database)
{
    char fullpath[200] = {0};
    sprintf(fullpath, "databases/%s", database);
    int check = mkdir(fullpath, 0777);

    if (!check)
    {
        FILE *fptr;
        fptr = fopen("databases/init/permission.table", "a+");

        if (fptr == NULL)
        {
            strcpy(pesan, "Error\n");
            return;
        }
        if (strcmp(currentUser, "root") == 0)
        {
            fprintf(fptr, "%s\t%s\n", currentUser, database);
            fclose(fptr);
        }
        else
        {
            fprintf(fptr, "%s\t%s\nroot\t%s\n", currentUser, database, database);
            fclose(fptr);
        }
        strcpy(pesan, "berhasil mazeee\n");
    }
    else
    {
        strcpy(pesan, "Unable to create directory\n");
        return;
    }
}
```
Ketika menggunakan command `CREATE DATABASE` akan memanggil fungsi di atas.
```shell
void createTable(char *table, char *column_name)
{
    FILE *fptr;
    char fullpath[500] = {0};
    sprintf(fullpath, "databases/%s/%s", currentDatabase, table);
    fptr = fopen(fullpath, "a+");
    if (fptr == NULL)
    {
        strcpy(pesan, "Error\n");

        return;
    }
    fprintf(fptr, "%s\n", column_name);
    fclose(fptr);
    strcpy(pesan, "berhasil mazeee\n");
}
```
Ketika menggunakan command `CREATE TABLE` akan memanggil fungsi di atas.
```shell
void dropDatabase(char *data, char *database)
{   
    tester=0;
    char fullpathCommand[2048] = {0};
    sprintf(fullpathCommand, "rm -r databases/%s", database);
    system(fullpathCommand);
    readDatabase("init", "permission.table");
    writeTable("databases/init/permission.table");
    strcpy(pesan, "berhasil mazeee\n");
}
```
Ketika menggunakan command `DROP DATABASE` akan memanggil fungsi di atas.
```shell
void dropTable(char *table)
{   
    tester=0;
    char fullpath[500] = {0};
    sprintf(fullpath, "databases/%s/%s", currentDatabase, table);
    if (remove(fullpath) == 0)
        strcpy(pesan, "berhasil mazeee\n");

    else
        strcpy(pesan, "Error\n");
}
```
Ketika menggunakan command `DROP TABLE` akan memanggil fungsi di atas.
```shell
void dropColumn(int position)
{
    char temp[200][2048] = {0};
    char hasil[200][2048] = {0};
    for (int i = 0; i < count_row; i++)
    {
        strcpy(temp[i], currData[i]);
    }

    for (int i = 0; i < count_row; i++)
    {

        char *token = strtok(temp[i], "\t");
        int count = 0;
        while (count < count_column)
        {
          
            if (count == position)
            {
                token = strtok(NULL, "\t");
                count++;
                continue;
                tester=1;
            }
            else
            {

                strcat(hasil[i], token);
                if (!strstr(hasil[i], "\n"))
                {
                    strcat(hasil[i], "\t");
                }
                token = strtok(NULL, "\t");
                count++;
            }
        }

        if (!strstr(hasil[i], "\n"))
        {   tester =1;
            strncpy(hasil[i], hasil[i], strlen(hasil[i]) - 1);
            strcat(hasil[i], "\n");
        }
    }
    for (int i = 0; i < count_row; i++)
    {
        strcpy(currData[i], hasil[i]);
    }
    for (int i = 0; i < 100; i++)
    {
        data_exist[i] = -2;
    }
}

void dropColumn2(int position)
{
    char tmp[2048] = {0};
    char hasil[2048] = {0};
    strcpy(tmp, column);
    char *token = strtok(tmp, "\t");
    int count = 0;
    while (count < count_column)
    {
        if (count == position)
        {
            token = strtok(NULL, "\t");
            count++;
            continue;
        }
        else
        {
            strcat(hasil, token);
            if (!strstr(hasil, "\n"))
            {
                strcat(hasil, "\t");
            }
            token = strtok(NULL, "\t");
            count++;
        }
    }

    if (!strstr(hasil, "\n"))
    {
        strncpy(hasil, hasil, strlen(hasil) - 1);
        strcat(hasil, "\n");
    }
    strcpy(column, hasil);
}
```
Ketika menggunakan command `DROP COLUMN` akan memanggil fungsi di atas.

## Point D
```shell
void insert(char *data, char *table)
{
    FILE *fptr;
    char fullpath[200] = {0};
    sprintf(fullpath, "databases/%s/%s", currentDatabase, table);
    fptr = fopen(fullpath, "a+");
    if (fptr == NULL)
    {
        strcpy(pesan, "Error\n");

        return;
    }
    fprintf(fptr, "%s\n", data);
    fclose(fptr);
    strcpy(pesan, "berhasil mazeee\n");
}
```
Ketika menggunakan command `INSERT INTO` akan memanggil fungsi di atas.
```shell

void updateData(int mode, int position, char *data_column, char *search_column, char *search_data)
{
  
    if (mode == 1)
    {
        char tmp[200][2048] = {0};
        char hasil[200][2048] = {0};
        for (int i = 0; i < count_row; i++)
        {
            strncpy(tmp[i], currData[i], strlen(currData[i]) - 1);
        }
        for (int i = 0; i < count_row; i++)
        {
            char *token = strtok(tmp[i], "\t");
            int j = 0;

            while (token != NULL)
            {

                if (j == position)
                {
                    strcat(hasil[i], data_column);
                    strcat(hasil[i], "\t");
                }
                else
                {
                    strcat(hasil[i], token);
                    strcat(hasil[i], "\t");
                }
                token = strtok(NULL, "\t");
                j++;
            }
            hasil[i][strlen(hasil[i]) - 1] = '\0';
            if (!strstr(hasil[i], "\n"))
            {
                strcat(hasil[i], "\n");

            }
        }
        debug=5;
        for (int i = 0; i < count_row; i++)
        {
            strcpy(currData[i], hasil[i]);

        }
        strcpy(pesan, "berhasil mazeee\n");
    }
    else if (mode == 2)
    {
        debug=1;
        char tmp[200][2048] = {0};
        char hasil[200][2048] = {0};
        for (int i = 0; i < count_row; i++)
        {
            strncpy(tmp[i], currData[i], strlen(currData[i]));

        }
        int j = 0;
        for (int i = 0; i < count_row; i++)
        {
            if (i == data_exist[j])
            {
                char *token = strtok(tmp[i], "\t");
                int k = 0;

                while (token != NULL)
                {
                    if (k == position)
                    {
                        strcat(hasil[i], data_column);
                        strcat(hasil[i], "\t");
                    }
                    else
                    {
                        strcat(hasil[i], token);
                        strcat(hasil[i], "\t");
                    }
                    token = strtok(NULL, "\t");
                    k++;
                }
                hasil[i][strlen(hasil[i]) - 1] = '\0';

                j++;
            }
            else
            {

                strcat(hasil[i], tmp[i]);
            }
            if (!strstr(hasil[i], "\n"))
            {
                strcat(hasil[i], "\n");

            }
        }
        for (int i = 0; i < count_row; i++)
        {
            strcpy(currData[i], hasil[i]);

        }
        strcpy(pesan, "berhasil mazeee\n");
    }
}
```
Ketika menggunakan command `UPDATE` akan memanggil fungsi di atas.
```shell
void deleteData(int mode, int position, char *fullpath)
{
    debug=0;
    if (mode == 1)
    {
        debug=0;
        FILE *fptr;
        fptr = fopen(fullpath, "w+");
        if (fptr == NULL)
        {
            strcpy(pesan, "Error\n");
            return;
        }
        fprintf(fptr, "%s", column);
        strcpy(pesan, "berhasil mazeee\n");
        fclose(fptr);
    }
    else if (mode == 2)
    {
        debug=1;
        FILE *fptr;
        int i = -1, j = 0;
        fptr = fopen(fullpath, "w+");
        if (fptr == NULL)
        {
            strcpy(pesan, "Error\n");
            return;
        }
        for (int i = -1; i < count_row; i++)
        {
            if (i == -1)
            {
                fprintf(fptr, "%s", column);
            }
            else
            {

                if (i == data_exist[j])
                {
                    j++;
                    continue;
                }
                fprintf(fptr, "%s", currData[i]);
            }
        }
        strcpy(pesan, "berhasil mazeee\n");
        fclose(fptr);
    }
}
```
Ketika menggunakan command `DELETE FROM` akan memanggil fungsi di atas.
```shell
void selectCommand(int mode)
{
    if (mode == 1)
    {
        debug=0;
        AddAjalah(mode);
    }
    else if (mode == 2)
    {
        debug=1;
        AddAjalah(mode);
    }
    else if (mode == 3)
    {
        debug=1;
        AddAjalah(mode);
    }
    else if (mode == 4)
    {
        debug=1;
        AddAjalah(mode);
    }
}
```
Ketika menggunakan command `SELECT` akan memanggil fungsi di atas.

## Point E
```shell
void masukMas(char *input)
{

    FILE *f = fopen("database.log", "a+");
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    fprintf(f, "%04d-%02d-%02d %02d:%02d:%02d:%s:%s\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, currentUser, input);
    fclose(f);
}
```
fungsi di atas akan mencatat setiap command yang kita masukan ke dalam file database.log